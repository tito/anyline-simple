package org.anyline.simple.postgis;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class GisApplication {
    private static JdbcTemplate jdbc;
    public static void main(String[] args) throws Exception{
        SpringApplication application = new SpringApplication(GisApplication.class);
        application.run(args);
    }
}
