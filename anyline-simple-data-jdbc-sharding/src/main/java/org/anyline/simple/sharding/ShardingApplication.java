package org.anyline.simple.sharding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"org.anyline"})

public class ShardingApplication {


	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(ShardingApplication.class);

		ConfigurableApplicationContext context = application.run(args);
	}
}
