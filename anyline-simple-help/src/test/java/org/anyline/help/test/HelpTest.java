package org.anyline.help.test;

import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;

public class HelpTest {
    public static void main(String[] args) {
        DataSet set = new DataSet();
        for(int i=0; i<10; i++){
            DataRow row = new DataRow();
            int s = i/3;
            row.put("CODE", "CODE:"+s+";NAME:"+s);
            row.put("G", s);
            row.put("ID", i);
            set.add(row);
        }
        DataSet groups = set.group("CODE","G");
        for(DataRow group:groups){
            DataSet items = group.getItems();
            System.out.println(items);
        }
    }
}
