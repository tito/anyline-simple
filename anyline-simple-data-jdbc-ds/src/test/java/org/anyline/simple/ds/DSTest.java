package org.anyline.simple.ds;

import com.zaxxer.hikari.HikariDataSource;
import org.anyline.data.jdbc.datasource.JDBCDatasourceHolder;
import org.anyline.entity.DataRow;
import org.anyline.metadata.Column;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.service.AnylineService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedHashMap;
import java.util.List;

@SpringBootTest
public class DSTest {

    @Autowired
    @Qualifier("anyline.service")
    private AnylineService service;
    @Test
    public void copyDatasource() throws Exception{
        List<String> list = JDBCDatasourceHolder.copy("default");
        System.out.println(list);
        list = JDBCDatasourceHolder.copy("default");
        System.out.println(list);
    }
    @Test
    public void copyTable() throws Exception{
        //测试表
        Table table = service.metadata().table("CRM_USER");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("CRM_USER");
        Column column = new Column("ID").autoIncrement(true).setType("int").primary(true);
        table.addColumn(column);
        table.addColumn("CODE","varchar(10)").setComment("编号");
        table.addColumn("NAME","varchar(10)").setComment("名称");
        service.ddl().create(table);

        //从第一个库中取出表结构
        table = service.metadata().table("CRM_USER");

        Table chk = ServiceProxy.service("sso").metadata().table("CRM_COPY");
        if(null != chk){
            ServiceProxy.service("sso").ddl().drop(chk);
        }
        table.setName("CRM_COPY");
        //在第二个库中创建
        ServiceProxy.service("sso").ddl().create(table);
    }


    @Test
    public void test() throws Exception{
        try {
            String url = "jdbc:mysql://localhost:33306/simple_sso?useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true";
            JDBCDatasourceHolder.reg("sso", "com.zaxxer.hikari.HikariDataSource", "com.mysql.cj.jdbc.Driver", url, "root", "root");
        }catch (Exception e){
            e.printStackTrace();
        }
        AnylineService service = ServiceProxy.service("sso");
        HikariDataSource ds = (HikariDataSource) JDBCDatasourceHolder.datasource("sso");
        for(int i=0;i <1000; i++){
            Table table = service.metadata().table( "sso_user", true);
            System.out.println("active:"+ds.getHikariPoolMXBean().getActiveConnections());
        }
    }

    @Test
    public void druid(){
        com.alibaba.druid.pool.DruidDataSource ds;
        try {
            String url = "jdbc:mysql://localhost:33306/simple_sso?useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true";
            JDBCDatasourceHolder.reg("sso", "com.alibaba.druid.pool.DruidDataSource", "com.mysql.cj.jdbc.Driver", url, "root", "root");
            com.alibaba.druid.pool.DruidDataSource h;
            LinkedHashMap<String,Table> tables = ServiceProxy.service("sso").metadata().tables();
            for(String table:tables.keySet()){
                Table t = ServiceProxy.service("sso").metadata().table(true, table, true);
                System.out.println(t);
            }
            Long[] ids = new Long[]{1L,3L};
            //service.deletes("crm_user","id", ids);
            ServiceProxy.deletes("crm_user","id", ids);
            DataRow row = ServiceProxy.service("sso").query("sso_user");
            System.out.println(row);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
