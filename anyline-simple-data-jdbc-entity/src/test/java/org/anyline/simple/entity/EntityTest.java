package org.anyline.simple.entity;

import org.anyline.adapter.KeyAdapter;
import org.anyline.data.jdbc.datasource.JDBCDatasourceHolder;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.data.run.Run;
import org.anyline.data.util.DataSourceUtil;
import org.anyline.entity.DataRow;
import org.anyline.service.AnylineService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.xml.crypto.Data;
import java.util.List;

@SpringBootTest(classes = EntityApplication.class)
public class EntityTest {

    @Autowired
    private AnylineService service;

    @Test
    public void update(){
        Department dept = new Department();
        dept.setId(11L);
        dept.setName("A");
        ConfigStore configs = new DefaultConfigStore();
        service.update("HR_DEPARTMENT<code>",dept, configs,"name");
        List<Run> runs = configs.runs();
        for (Run run:runs){
            System.out.println(run.getFinalUpdate(false));
        }
        DataRow row = new DataRow(KeyAdapter.KEY_CASE.SRC);
        row.put("ID", "1");
        row.put("code", "2");
        row.put("name", "2n");
        service.update("HR_DEPARTMENT<code>", row,  "name");

    }

}
