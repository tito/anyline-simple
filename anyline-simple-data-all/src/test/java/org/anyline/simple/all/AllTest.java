package org.anyline.simple.all;

import com.alibaba.druid.pool.DruidDataSource;
import org.anyline.adapter.KeyAdapter;
import org.anyline.data.datasource.DatasourceHolder;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.data.run.Run;
import org.anyline.data.runtime.RuntimeHolder;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.metadata.Column;
import org.anyline.metadata.Database;
import org.anyline.metadata.PrimaryKey;
import org.anyline.metadata.Table;
import org.anyline.metadata.type.DatabaseType;
import org.anyline.net.HttpResponse;
import org.anyline.proxy.ServiceProxy;
import org.anyline.service.AnylineService;
import org.anyline.util.BasicUtil;
import org.anyline.util.ConfigTable;
import org.anyline.util.FileUtil;
import org.anyline.util.NumberUtil;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

@SpringBootTest(classes = BootApplication.class)
public class AllTest {
    @Autowired
    private AnylineService service          ;
    private static Logger log = LoggerFactory.getLogger(AllTest.class);

    public static void main(String[] args) {
        int qty =0;
        while (true) {
            DataSet set = new DataSet();
            for (int i = 0; i < 1000; i++) {
                DataRow row = new DataRow();
                row.put("ID", NumberUtil.random(0, 10));
                set.add(row);
            }
            set.asc("ID");
            System.out.println(qty++);
        }

    }

    @Test
    public void databases() throws SQLException {
        LinkedHashMap<String, Database> databases = service.metadata().databases();
        for(Database database:databases.values()){
            log.warn("数据库:{}", database.getName());
        }
    }
    @Test
    public void sort() throws Exception{
        Table table = service.metadata().table("crm_user");
        // table.getColumn("CODE").drop();
        table.sort();
        service.ddl().alter(table);
    }
    @Test
    public void truncate() throws Exception{
        ConfigTable.IS_SQL_DELIMITER_OPEN =true;
        service.truncate("crm_user");
    }
    @Test
    public void virtual() throws Exception{
        AnylineService service = ServiceProxy.service(DatabaseType.MySQL);
        ConfigStore configs = new DefaultConfigStore();
        service.querys("crm_user", configs);
        List<Run> runs = configs.runs();
        for(Run run:runs){
            System.out.println(run.getFinalQuery());
        }
        Table table = new Table("crm_user");
        table.addColumn("ID", "INT");
        service.ddl().create(table);
        List<String> ddls = table.ddls();
        for (String ddl:ddls){
            System.out.println(ddl);
        }
    }
    /**
     * 创建表
     * @throws Exception
     */
    @Test
    public void createTable() throws Exception{
        //有些数据库支持IF EXISTS可以直接drop
        //有些不支持需要先检测表是否存在
        Table table = new Table("FI_ORDER");
        if(service.metadata().exists(table)){
            service.ddl().drop(table);
        }

        //定义表 默认当前连接的catalog与schema
        table = new Table<>("FI_ORDER").setComment("财务_订单");
        //添加列
        Column column = new Column("id","bigint").autoIncrement(true).primary(true).setComment("编号");
        table.addColumn(column);
        //也可以这样添加列
        table.addColumn(  "ITEM_QTY",  "int").setComment("");

        table.addColumn("CREATE_TIME", "datetime").setDefaultCurrentDateTime();

        service.ddl().save(table);
        table = service.metadata().table("FI_ORDER");
        table.addColumn("NEW_COLUMN", "varchar(10)");
        service.ddl().save(table);
    }

    /**
     * 主键
     * @throws Exception
     */
    @Test
    public void primary() throws Exception{
        Table table = new Table("FI_ORDER_PK");
        if(service.metadata().exists(table)){
            service.ddl().drop(table);
        }
        table = new Table("SD_ORDER_PK");
        if(service.metadata().exists(table)){
            service.ddl().drop(table);
        }
        table = new Table("PP_ORDER_PK");
        if(service.metadata().exists(table)){
            service.ddl().drop(table);
        }


        table = new Table<>("FI_ORDER_PK").setComment("财务_订单1");
        //可以在列上设置主键标识
        table.addColumn("id","bigint").autoIncrement(true).primary(true).setComment("编号");
        table.addColumn(  "ITEM_QTY",  "int").setComment("");
        service.ddl().create(table);

        //也可以单独添加主键
        table = new Table<>("SD_ORDER_PK").setComment("分销_订单2");
        Column col = table.addColumn("id","bigint").setComment("主键");
        PrimaryKey pk = new PrimaryKey() ;
        pk.addColumn(col);
        table .setPrimaryKey(pk) ;
        service.ddl().create(table);

        //因大部分数据库 要求自增列必须在主键上 所以设置了自增的同时也设置成主键
        table = new Table<>("PP_ORDER_PK").setComment("生产_订单3");
        table.addColumn("id","bigint").autoIncrement(true).setComment("主键");
        service.ddl().create(table);
    }
    /**
     * 查询所有表
     * @throws Exception
     */
    @Test
    public void tables() throws Exception{
        for(int i=0;i <10; i++) {
            LinkedHashMap<String, Table> tables = service.metadata().tables();
            for (String table : tables.keySet()) {
                System.out.println(service.metadata().table(table, true));
            }
        }
    }

    /**
     * 验证数据源是否可用
     */
    @Test
    public void validate() {
        boolean result = DatasourceHolder.validate("pg");
        log.warn("连接状态:{}",result);
        AnylineService pg = ServiceProxy.service("pg");
        log.warn("连接状态:{}", pg.validate());
        //这样会异常异常，可以捕捉一下用来显示具体异常
        try {
            pg.hit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /**
     * 临时数据源
     * @throws Exception
     */
    @Test
    public void temporary() throws Exception{
        for(int i=0; i<10; i++) {
            String url = "jdbc:mysql://localhost:33306/simple?useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true";
            DruidDataSource ds = new DruidDataSource();
            ds.setUrl(url);
            ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
            ds.setUsername("root");
            ds.setPassword("root");
            ds.setConnectionErrorRetryAttempts(3);
            ds.setBreakAfterAcquireFailure(true);
            ds.setConnectTimeout(3000);
            ds.setMaxWait(30000);
            service = ServiceProxy.temporary(ds);
            boolean status = service.validate();
            log.warn("连接状态:{}", status);
            LinkedHashMap<String, Table> tables = service.metadata().tables();
            for (String table : tables.keySet()) {
                System.out.println(service.metadata().table(table, true));
            }
        }
    }
    /**
     * 复制数据源(与数据源在同一个服务器上的其他数据库)
     * @throws Exception
     */
    @Test
    public void copyDatasource() throws Exception{
        //根据数据源 创建当前数据源所在服务器的其他数据库相关的数据源
        //不提供参数复制默认数据源 default
        //当前服务器上还有simple_crm/simple_sso，以下会再创建两个数据源default_simple_crm/default_simple_sso
        //注意数据名会转成小写、但前缀不会转小写，项目中定义的是什么就是什么
        List<String> list = DatasourceHolder.copy();
        log.warn("创建数据源:{}", list);
        //key已存在，不会重复创建
        list = DatasourceHolder.copy("default");
        log.warn("创建数据源:{}", list);
        log.warn("全部数据源:{}", RuntimeHolder.all().keySet());
        ServiceProxy.service("default_simple_sso").query("sso_user");
        //注销主数据源时  复制出来的子源也一块注销
        DatasourceHolder.destroy("default");

        log.warn("全部数据源:{}", RuntimeHolder.all().keySet());

    }
    @Test
    public void copyTable() throws Exception{
        //测试表
        Table table = service.metadata().table("CRM_USER");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("CRM_USER");
        Column column = new Column("ID").autoIncrement(true).setType("int").primary(true);
        table.addColumn(column);
        table.addColumn("CODE","varchar(10)").setComment("编号");
        table.addColumn("NAME","varchar(10)").setComment("名称");
        service.ddl().create(table);

        //从第一个库中取出表结构
        table = service.metadata().table("CRM_USER");

        Table chk = ServiceProxy.service("sso").metadata().table("CRM_COPY");
        if(null != chk){
            ServiceProxy.service("sso").ddl().drop(chk);
        }
        table.setName("CRM_COPY");
        //在第二个库中创建
        ServiceProxy.service("sso").ddl().create(table);
    }

    @Test
    public void es_(){
        RestClient client = (RestClient)ServiceProxy.service("es").getDao().runtime().getProcessor();
        String method = "GET";
        String endpoint = "_cat/indices";
        Request request = new Request(
                method,
                endpoint);
        HttpResponse response = exe(client, request);
        if(response.getStatus() == 200 ||  response.getStatus() == 201){
            //DataRow row = DataRow.parseJson(response.getText());
            String txt =response.getText();
            System.out.println(txt);
            String[] lines =txt.split("\n");
            for(String line:lines){
                String[] cols = BasicUtil.compress(line).split(" ");
                Table table = new Table(cols[2]);
            }
        }
    }
    private HttpResponse exe(RestClient client, Request request){
        HttpResponse result = new HttpResponse();
         try {
            Response response = client.performRequest(request);
            int status = response.getStatusLine().getStatusCode();
            result.setStatus(status);
            //{"_index":"index_user","_id":"102","_version":3,"result":"updated","_shards":{"total":2,"successful":2,"failed":0},"_seq_no":9,"_primary_term":1}
            String content = FileUtil.read(response.getEntity().getContent()).toString();
            result.setText(content);
            log.warn("[status:{}]", status);
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
    @Test
    public void es() throws Exception{
        Table table = ServiceProxy.service("es").metadata().table("es_docx");
        if(null !=table) {
            ServiceProxy.service("es").ddl().drop(new Table("es_docx"));
        }
        table = new Table("es_docx");
        //table.addColumn("_id"           ,  "keyword"    ); // 类别_业务主键
        table.addColumn("id"            	, "integer"     ).setStore(true); // 业务主键
        table.addColumn("type"          	, "integer"     ).setStore(true); // 0:法规 1:问答
        table.addColumn("title"         	, "text"        ).setStore(true).setAnalyzer("us_max_word").setSearchAnalyzer("us_smart"); // 标题
        table.addColumn("content"       	, "text"        ).setStore(false).setAnalyzer("us_max_word").setSearchAnalyzer("us_smart");// 内容
        table.addColumn("summary"       	, "text"        ).setStore(true).setAnalyzer("us_max_word").setSearchAnalyzer("us_smart"); // 概要
        table.addColumn("file_code"     	, "text"        ).setStore(true).setAnalyzer("us_max_word").setSearchAnalyzer("us_smart"); // 文号
        table.addColumn("query_file_code" , "keyword"     ).setStore(true).setIgnoreAbove(200); // 模糊搜索文号
        table.addColumn("sector"        	, "keyword"     ).setStore(true); // 一级部门
        table.addColumn("sub_sector"    	, "keyword"     ).setStore(true); // 二级部门
        table.addColumn("yyyy"          	, "integer"     ).setStore(true); // 年份
        table.addColumn("tax_ids"       	, "integer"     ).setStore(true); // 税种
        table.addColumn("tax_nms"       	, "keyword"     ).setStore(true); // 税种
        table.addColumn("industry_ids"  	, "integer"     ).setStore(true); // 行业
        table.addColumn("industry_nms"  	, "keyword"     ).setStore(true); // 行业
        table.addColumn("subject_ids"   	, "integer"     ).setStore(true); // 专题
        table.addColumn("subject_nms"   	, "keyword"     ).setStore(true); // 专题
        table.addColumn("vip_lvl"       	, "integer"     ).setStore(true); // 要求VIP级别
        table.addColumn("keywords"      	, "keyword"     ).setStore(true).setIgnoreAbove(20); // 关键字
        table.addColumn("effect"        	, "integer"     ).setStore(true); // 效力
        table.addColumn("pub_ymd"       	, "date"        ).setStore(true); // 发布日期
        table.addColumn("update_ymd"    	, "keyword"     ).setStore(true); // 更新日期
        table.addColumn("law_qty"       	, "integer"     ).setStore(true); // 相关法规
        table.addColumn("qa_qty"        	, "integer"     ).setStore(true); // 相关问答
        table.addColumn("file_qty"      	, "integer"     ).setStore(true); // 相关文件
        table.addColumn("read_qty"      	, "integer"     ).setStore(true); // 阅读量

        DataRow settings = new DataRow(KeyAdapter.KEY_CASE.SRC);
        DataRow analysis = settings.put("analysis");
        analysis.put("filter").put("us_synonym_filter")
                .set("type","synonym_graph")
                .set("synonyms_path", "analysis/us_synonym.txt");
        DataRow analyzer = analysis.put("analyzer");
        analyzer.put("us_max_word")
                .set("tokenizer","ik_max_word")
                .set("filter",new String[]{"us_synonym_filter"});
        analyzer.put("us_smart")
                .set("tokenizer","ik_smart")
                .set("filter",new String[]{"us_synonym_filter"});

        table.setExtend(settings);
        ServiceProxy.service("es").ddl().create(table);
    }
}
