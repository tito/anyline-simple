package org.anyline.simple.dml;

import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.service.AnylineService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DMLTest {
    private Logger log = LoggerFactory.getLogger(DMLTest.class);
    @Autowired
    private AnylineService service          ;
    @Test
    public void or(){

        String val = "admin";
        String id = "123";
        ConfigStore configs = new DefaultConfigStore();
        configs.ne("ID", id);
        ConfigStore act = new DefaultConfigStore();
        act.and("LOGIN_ACCOUNT", val).or("LOGIN_MOBILE", val).or("LOGIN_IDCARD", val).or("LOGIN_MAIL", val);
        configs.and(act);

        service.query("PW_USER", configs );
    }
}
