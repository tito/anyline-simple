package org.anyline.simple.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeneratorApplication {

    public static void main(String[] args){
        SpringApplication application = new SpringApplication(GeneratorApplication.class);
        application.run(args);


    }
}
