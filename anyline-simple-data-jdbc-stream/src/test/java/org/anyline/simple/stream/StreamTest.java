package org.anyline.simple.stream;

import org.anyline.data.handler.DataRowHandler;
import org.anyline.data.handler.EntityHandler;
import org.anyline.data.handler.MapHandler;
import org.anyline.data.handler.ResultSetHandler;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.service.AnylineService;
import org.anyline.util.BeanUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.ResultSet;
import java.util.Map;

@SpringBootTest(classes = StreamApplication.class)
public class StreamTest {
    @Autowired
    private AnylineService service          ;

    private void init(){
        long fr = System.currentTimeMillis();
        DataSet set = new DataSet();
        for(int i=0; i<500; i++){
            DataRow row = new DataRow();
            row.put("ID", fr+i);
            row.put("CODE", "C"+i);
            set.add(row);
        }
        service.insert("CRM_USER", set);
    }
    @Test
    public void maps(){
        init();
        MapHandler handler = new MapHandler() {
            @Override
            public boolean read(Map<String, Object> map) {
                System.out.println(map);
                return false;
            }
        };
        service.maps("CRM_USER", handler, "ID IN(1,2,3)");

        //或者放到 ConfigStore中
        ConfigStore configs = new DefaultConfigStore();
        configs.stream(handler);
        configs.and("CODE IS NOT NULL");

        service.maps("CRM_USER", configs, "ID IN(1,2,3)");
    }
    @Test
    public void row(){
        init();

        DataRowHandler handler = new DataRowHandler() {
            @Override
            public boolean read(DataRow row) {
                System.out.println(row);
                return true;
            }
        };
        service.querys("CRM_USER", handler, "ID:>1");
    }
    @Test
    public void entity(){
        init();
        EntityHandler<User> handler = new EntityHandler<User>() {
            @Override
            public boolean read(User user) {
                System.out.println(BeanUtil.object2json(user));
                return false;
            }

        };
        service.selects("CRM_USER", User.class, handler, "ID:>1");
    }
    @Test
    public void resultSet(){
        init();
        ResultSetHandler handler = new ResultSetHandler() {
            @Override
            public boolean read(ResultSet set) {
                try {
                    System.out.println(set.getObject(1));
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            }
        };
        service.querys("CRM_USER", handler, "ID:>1");
    }
}
