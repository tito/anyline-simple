package org.anyline.simple.spider;

import org.anyline.adapter.KeyAdapter;
import org.anyline.entity.DataRow;
import org.anyline.net.HttpBuilder;
import org.anyline.net.HttpClient;
import org.anyline.net.HttpResponse;
import org.anyline.net.HttpUtil;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SpiderTest {
    public static void main(String[] args) {
        System.out.println(HttpUtil.get("http://gitee.com/anyline/anyline/stargazers").getText());
       // login();
    }
    //保持会话状态
    public static void login(){
        String url = "http://www.baidu.com";
        DataRow parameters = new DataRow(KeyAdapter.KEY_CASE.SRC);
        HttpClient client = HttpBuilder.init()
                .setParams(parameters)
                .build();
        client.setUrl(url);
        client.setAutoClose(false);
        HttpResponse response = client.post();
        System.out.println("登录结果:"+response.getText());
        parameters = new DataRow(KeyAdapter.KEY_CASE.SRC);
        //查询参数
        client.setUrl(url);
        client.setParams(parameters);
        response=client.post();
        System.out.println("库存查询："+response.getText());
    }

}