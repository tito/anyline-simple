package org.anyline.simple.mongodb;

import org.anyline.data.mongodb.entity.MongoDataRow;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.entity.Compare;
import org.anyline.entity.DataSet;
import org.anyline.entity.DefaultPageNavi;
import org.anyline.entity.PageNavi;
import org.anyline.proxy.ServiceProxy;
import org.anyline.service.AnylineService;
import org.anyline.util.ConfigTable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MongoTest {
    @Test
    public void init(){
        String table =  "crm_user6";
        ConfigTable.PRIMARY_GENERATOR_SNOWFLAKE_ACTIVE = true;
        AnylineService service = ServiceProxy.service("mongo");
        DataSet set = new DataSet();
        for(int i=0; i<10;i ++){
            MongoDataRow row = new MongoDataRow();
            //row.put("_id", fr+i);
            row.put("name", "USER_"+i);
            row.put("code", "CODE"+i);
            row.put("age",i);
            set.add(row);
        }
        MongoDataRow row = new MongoDataRow();
        //row.put("_id", fr+i);
        row.put("name", "USER_"+1);
        row.put("code", "CODE"+1);
        row.put("age",1);
        ConfigTable.DEFAULT_MONGO_ENTITY_CLASS = MongoDataRow.class;
        //insert时 _id不可以重复
        //插入多行
        service.insert(table, set);
        Assertions.assertEquals(10, set.size());
        row = (MongoDataRow) service.query(table);

        //更新一行 根据_id
        row.put("name", "test");
        service.update(table, row);

        row = (MongoDataRow) service.query(table ,"name:test");
        System.out.println("修改结果:"+row);

        //根据条件更新
        service.update(table, row, new DefaultConfigStore().and(Compare.GREAT, "age", 1));
        System.out.println(service.querys(table));
        service.delete(row);

        service.delete(table, "_id", "1");

        service.deletes(table, "code", "CODE1","CODE2");

        //查询整个表
        set = service.querys(table);
        //查部分列 主键_id默认查询
        set = service.querys("crm_user2(age,code)");
        System.out.println("查询结果:"+set.size());
        System.out.println("查询结果结构:"+set.getRow(0));
        //不查AGE和_id
        set = service.querys("crm_user2(!age,!_id)");

        System.out.println("查询结果:"+set.size());
        if(set.size()>0) {
            System.out.println("查询结果结构:" + set.getRow(set.size() - 1));
        }
        //按条件查询
        set = service.querys(table,
                new DefaultConfigStore().and(Compare.GREAT,"age", 1)
                        .and(Compare.LESS_EQUAL, "age", 5)
                        .in("age", new Integer[]{1,2})
                ,"name:%1%","code:%1");

        System.out.println("查询结果:"+set.size());
        if(set.size()>0) {
            System.out.println("查询结果结构:" + set.getRow(set.size() - 1));
        }
        //分页
        set = service.querys(table, new DefaultPageNavi(2).setPageRows(2));
        PageNavi navi = set.getNavi();
        System.out.println("查询结果: 总行数="+navi.getTotalRow()+",总页数:"+navi.getTotalPage()+",当前页:"+navi.getCurPage()+",结果集:"+set);
    }
}
